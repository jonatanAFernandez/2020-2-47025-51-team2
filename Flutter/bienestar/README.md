# bienestar

A new Flutter project.

## Notas para mi
- limpiar el editableRangoHorarioContainer

## Problemas con autenticacion
1. Microsoft no sirve en Flutter directamente
```
  var provider = new OAuthProvider('microsoft.com');
  el redirect claramente no sirve
  auth.signInWithRedirect(provider);
 //el redirect claramente no sirve
 auth.signInWithPopup(provider);
 //el withPopup tampoco. Ambos son exclusivos para plataforma WEB
```


2. La instancia de FirebaseAuth recuerda el último usuario. Sign-out es necesario siempre. 

## Las reglas de algún modo cambian por cloud_firestore: 0.13.7" -> 0.14.1+3"
- la excepción que me sale es: 
                  The caller does not have permission to execute the specified operation
- la configuración (por defecto del proyecto es):
```
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if    
            request.time < timestamp.date(2020, 10, 22);
    }
  }
}
```

## Preguntas
- por qué en web es FirabaseFirestore.instance y en móvil firestore.instance [x]

## Estilos


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
