import 'package:bienestar/Modulos/ModuloHorario.dart';
import 'package:flutter/material.dart';
import '../models/Espacio.dart';
import '../models/EspacioFormControllers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class EspacioDetalle extends StatefulWidget {
  //básicamente para almacenar la referencia
  final Espacio espacio;

  EspacioDetalle(this.espacio);

  // Aquí encapsulo el procedimiento en Firestore
  // Modificar solo aquí:

  @override
  EspacioDetalleState createState() {
    return EspacioDetalleState();
  }
}

class EspacioDetalleState extends State<EspacioDetalle> {
  var _formKey = GlobalKey<FormState>();
  final firestore = FirebaseFirestore.instance;
  Espacio espacio;

  EspacioFormControllers campos = EspacioFormControllers();
  ModuloHorario horario = ModuloHorario();
  // Validators
  // ignore: top_level_function_literal_block

  @override
  Widget build(BuildContext context) {
    setState(() {
      espacio = widget.espacio;
    });

    return Scaffold(
        appBar: AppBar(
          title: Text("Ver espacio"),
        ),
        body: ListView(children: [
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                darTitulo('Nombre'),
                Divider(
                  height: 10,
                  thickness: 5,
                  indent: 20,
                  endIndent: 20,
                ),
                darCuerpo(widget.espacio.nombre),
                darTitulo('Descripicón'),
                Divider(
                  height: 10,
                  thickness: 5,
                  indent: 20,
                  endIndent: 20,
                ),
                darCuerpo(widget.espacio.descripcion),
                darTitulo('Recomendaciones'),
                Divider(
                  height: 10,
                  thickness: 5,
                  indent: 20,
                  endIndent: 20,
                ),
                darCuerpo(widget.espacio.recomendaciones),
                darTitulo('Disponibilidad Horaria'),
                Divider(
                  height: 10,
                  thickness: 5,
                  indent: 20,
                  endIndent: 20,
                ),
                darCuerpo('TODO')
              ],
            ),
          )
        ]));
  }
}

darTitulo(var text) {
  return Container(
      padding: EdgeInsets.all(20),
      alignment: Alignment.bottomLeft,
      child: Text(text,
          style: TextStyle(
              fontFamily: "Raleway",
              fontSize: 24,
              fontWeight: FontWeight.w400)));
}

darCuerpo(var texto) {
  return Container(
    padding: EdgeInsets.all(20),
    alignment: Alignment.bottomLeft,
    child: Text(texto,
        style: TextStyle(
            fontFamily: "Lato",
            fontSize: 17,
            fontWeight: FontWeight.w400,
            letterSpacing: 0.5)),
  );
}
