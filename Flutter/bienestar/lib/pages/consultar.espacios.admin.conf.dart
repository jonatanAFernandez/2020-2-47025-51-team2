import 'package:flutter/material.dart';
import '../models/Reserva.dart' show Reserva;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:convert';
import './editar.espacio.dart' show EditarEspacioForm;
// import './consultar.espacios.detalle.dart' show VistaDetalle;

class AdminOptions extends StatelessWidget {
  // Fila de opciones del administrador.
  // La fila de botones se ponen dentro de un ListTile
  // Recibe un espacio
  final _espacio;
  AdminOptions(this._espacio);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        AdminEspaciosDeleteButton(_espacio),
        AdminEspaciosDetalleButton(EditarEspacioForm(_espacio)),
        // AdminEspaciosDetalleButton(VistaDetalle(espacioDetalle: _espacio))
      ],
    );
  }
}

class AdminEspaciosDeleteButton extends RaisedButton {
  final _espacio;

  AdminEspaciosDeleteButton(this._espacio);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        onPressed: () {
          var log = _espacio.toJson();
          _espacio.reference.delete();
          debugPrint('deleted from firestore $log');
        },
        child: Text('Borrar'));
  }
}

class AdminEspaciosDetalleButton extends RaisedButton {
  final _route;

  AdminEspaciosDetalleButton(this._route);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => _route),
          );
        },
        child: Text('Editar'));
  }
}

// class ReservarElementButton extends RaisedButton {
//   final _element;

//   ReservarElementButton(this._element);

//   @override
//   Widget build(BuildContext context) {
//     return RaisedButton(
//         onPressed: () {
//           Reserva nuevaReserva =
//               Reserva.conAgendamiento(_element, 'admin', DateTime.now());
//           print(nuevaReserva.toJson()['horaAgendada'].runtimeType);
//           Firestore.instance.collection('reservas').add(nuevaReserva.toJson());

//           debugPrint(
//               'wrote ${nuevaReserva.toJson()['elemento']['nombre']}, ${nuevaReserva.toJson()['usuario']} to Firestore');

//           Scaffold.of(context)
//               .showSnackBar(SnackBar(content: Text(jsonEncode(nuevaReserva))));
//         },
//         child: Text('Reservar'));
//   }
// }
// class ReservarElementButton extends RaisedButton {
//   final _element;

//   ReservarElementButton(this._element);

//   @override
//   Widget build(BuildContext context) {
//     return RaisedButton(
//         onPressed: () {
//           Reserva nuevaReserva =
//               Reserva.conAgendamiento(_element, 'admin', DateTime.now());
//           print(nuevaReserva.toJson()['horaAgendada'].runtimeType);
//           FirebaseFirestore.instance
//               .collection('reservas')
//               .add(nuevaReserva.toJson());

//           debugPrint(
//               'wrote ${nuevaReserva.toJson()['elemento']['nombre']}, ${nuevaReserva.toJson()['usuario']} to Firestore');

//           Scaffold.of(context)
//               .showSnackBar(SnackBar(content: Text(jsonEncode(nuevaReserva))));
//         },
//         child: Text('Reservar'));
//   }
// }
