import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bienestar/main.dart';
import '../models/Reserva.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:bienestar/estilos_improvisados.dart';

class ReservaActiva extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<ReservaActiva> {
  Reserva reserva;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: FutureBuilder(
      future: FirebaseFirestore.instance
          .collection("beneficiarios")
          .doc(auth.currentUser.uid)
          .get(),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data["reservaActiva"] != false) {
          debugPrint(
              "Se detectó una reserva activa. Procediendo a buscar la reserva ${snapshot.data["reservaActiva"].id}");
          return FutureBuilder(
              future: FirebaseFirestore.instance
                  .collection("reservas")
                  .doc(snapshot.data["reservaActiva"].id)
                  .get(),
              builder: (context, value) {
                if (value.hasData) {
                  debugPrint(
                      "Se encontró la reserva ${value.data.id}. Procediendo a construir el widget");

                  reserva = Reserva.fromSnapshot(value.data);
                  debugPrint(jsonEncode(reserva.elementoReservado.toJson()));

                  return _mostrarReservaActiva(reserva);
                } else {
                  debugPrint("Parece que hubo un problema ");
                  return Center(child: CircularProgressIndicator());
                }
              });
        } else if (snapshot.hasData &&
            snapshot.data["reservaActiva"] == false) {
          return Center(child: Text("No tienes una reserva activa"));
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner.
        return Center(child: CircularProgressIndicator());
      },
    ));
  }
}

_mostrarReservaActiva(Reserva reserva) {
  return Container(
      child: Center(
          child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text("Reserva", style: tit_seccion1),
      Padding(padding: EdgeInsets.all(5)),
      Text(
        reserva.elementoReservado.nombre,
        style: tit_display,
      ),
      Padding(padding: EdgeInsets.all(15)),
      Text("Inicio", style: tit_seccion1),
      Text("${DateFormat('MMM d h:mm a').format(reserva.horaAgendada)}",
          style: tit_display),
      Padding(padding: EdgeInsets.all(5)),
      Text("Fin", style: tit_seccion1),
      Text(
          "${DateFormat('h:mm a').format(reservaMasTiempoLimite(reserva.horaAgendada))}",
          style: tit_display),
      Padding(padding: EdgeInsets.all(15)),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Te esperamos ", style: tit_seccion1),
          Icon(Icons.favorite, color: Color(0xffee4a52))
        ],
      ),
    ],
  )));
}

reservaMasTiempoLimite(DateTime horaAgendada) {
  return DateTime(horaAgendada.year, horaAgendada.month, horaAgendada.day,
      horaAgendada.hour + 1, horaAgendada.minute);
}
