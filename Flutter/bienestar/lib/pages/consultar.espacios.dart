import 'package:bienestar/pages/consultar.espacios.bene.conf.dart';
import 'package:flutter/material.dart';
import '../models/Espacio.dart' show Espacio;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'consultar.espacios.admin.conf.dart' show AdminOptions;
import '../coreWidgets/ItemLista.dart';
import 'package:bienestar/main.dart';

class ConsultarEspacios extends StatefulWidget {
  // Creo un listView a partir de un StreamBuilder
  // Cada ListItem tiene una fila con configuración 'AdminOptions'

  final GlobalKey<ScaffoldState> scaffold;
  ConsultarEspacios(this.scaffold);
  @override
  ConsultarEspaciosState createState() {
    return ConsultarEspaciosState();
  }
}

class ConsultarEspaciosState extends State<ConsultarEspacios> {
  static const db_string = "[Consultar Espacios]";
  @override
  Widget build(BuildContext context) {
    debugPrint('ConsultarEspacios view created');

    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    //el StreamBuilder chequea si los snapshots de la coleccion
    //tienen datos. Si sí tienen datos,entonces construye una lista a partir de
    //sus *documentos*

    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance.collection('espacios').snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          throw snapshot.error;
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return LinearProgressIndicator();
        }

        return _buildList(context, snapshot.data.docs);
      },
    );
  }

  Widget _buildList(
      BuildContext context, List<QueryDocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: snapshot.map((data) => _buildListItem(context, data)).toList(),
    );
  }

  /// Si es Admin, retorna [AdminOptions]; si no, retorna [BeneOptions]
  Widget _buildListItem(BuildContext context, QueryDocumentSnapshot data) {
    //este es el widget de cada elemento individual,
    final espacio = Espacio.fromSnapshot(data);

    if (auth.currentUser.email == 'admin@bienestar.com') {
      debugPrint("${ConsultarEspaciosState.db_string} Admin options loaded");
      return Padding(
        key: ValueKey(espacio.nombre),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        // El basicListTile muestra atributos básicos
        // y luego las opciones del rol
        child: ItemLista(espacio, roleOptions: AdminOptions(espacio)),
      );
    } else {
      debugPrint(
          "${ConsultarEspaciosState.db_string} Beneficiario options loaded");
      return Padding(
        key: ValueKey(espacio.nombre),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        // El basicListTile muestra atributos básicos
        // y luego las opciones del rol
        child: ItemLista(espacio,
            roleOptions: BeneOptions(espacio, widget.scaffold)),
      );
    }
  }
}
