import 'package:bienestar/pages/hacerReserva.dart';
import 'package:flutter/material.dart';

import 'espacio.detalle.dart';
// import './consultar.espacios.detalle.dart' show VistaDetalle;

class BeneOptions extends StatelessWidget {
  // Fila de opciones del administrador.
  // La fila de botones se ponen dentro de un ListTile
  // Recibe un espacio
  final _espacio;
  final GlobalKey<ScaffoldState> scaffold;
  BeneOptions(this._espacio, this.scaffold);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        BeneReservarElementButton(_espacio, scaffold),
        BeneEspaciosDetalleButton(EspacioDetalle(_espacio)),
      ],
    );
  }
}

/// Redirige a ver el detalle del espacio
class BeneEspaciosDetalleButton extends RaisedButton {
  final _route;

  BeneEspaciosDetalleButton(this._route);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => _route),
          );
        },
        child: Text('Ver'));
  }
}

/// Redirige al forumario [HacerReserva]
class BeneReservarElementButton extends RaisedButton {
  final _element;
  final GlobalKey<ScaffoldState> scaffoldHome;
  BeneReservarElementButton(this._element, this.scaffoldHome);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => HacerReserva(_element, scaffoldHome)),
          );
        },
        child: Text('Reservar'));
  }
}
