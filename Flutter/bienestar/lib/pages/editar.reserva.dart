import 'dart:convert';
import 'package:bienestar/models/DisponibilidadPorDia.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../models/Reserva.dart' show Reserva;
import '../coreWidgets/ControllerFormField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../Modulos/ModuloInvitados.dart';

class EditarReservaForm extends StatefulWidget {
  final Reserva reserva;

  EditarReservaForm(this.reserva);

  /// Encapsulo el procedimiento en Firestore. Actualizo los datos de la Reserva con
  /// los datos manipulados en el formulario.
  Future<void> firestoreUpdateData(Reserva reserva) {
    return reserva.reference
        .update(reserva.toJson())
        .then((value) => debugPrint('Update completed'));
  }

  @override
  EditarReservaFormState createState() {
    return EditarReservaFormState();
  }
}

class EditarReservaFormState extends State<EditarReservaForm> {
  var _formKey = GlobalKey<FormState>();

  final firestore = FirebaseFirestore.instance;
  ModuloInvitados moduloInvitados;
  Reserva reserva;
  TextEditingController beneficiarioController = TextEditingController();
  TextEditingController elementoController = TextEditingController();

  Function validarCampoVacio = (String value) {
    return null;
  };

  @override
  Widget build(BuildContext context) {
    debugPrint('Editar-espacio view created');
    DisponibilidadPorDia disp = DisponibilidadPorDia.fromMap(
        widget.reserva.elementoReservado.horario.dias["lunes"]);
    debugPrint(jsonEncode(disp));

    setState(() {
      widget.reserva.elementoReservado.darBloquesHorarios;
      reserva = widget.reserva;
    });

    return Scaffold(
        appBar: AppBar(
          title: Text("Editar Reserva"),
        ),
        body: ListView(children: [
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                ControllerFormField(widget.reserva.beneficiario,
                    beneficiarioController, validarCampoVacio),
                ListTile(
                  title: Text(widget.reserva.elementoReservado.nombre),
                  subtitle: Text('Elemento Reservado'),
                ),
                SizedBox(
                  child: CupertinoDatePicker(
                      onDateTimeChanged: (date) =>
                          beneficiarioController.text = date.toString()),
                  height: 50,
                ),
                // ModuloBloqueChips<DisponibilidadPorDia>(
                //     widget.reserva.elementoReservado.darBloquesHorarios()),

                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          // Preparar datos para instanciar un Espacio

                          reserva.setBeneficiario(beneficiarioController.text);

                          debugPrint('Attempted update document:');
                          debugPrint(jsonEncode(reserva), wrapWidth: 1000);

                          // Scaffold.of(context).showSnackBar(
                          //     SnackBar(content: Text(jsonEncode(espacio))));
                          return widget.firestoreUpdateData(reserva);
                        }
                      },
                      child: Text('Modificar reservas'),
                    )),
              ],
            ),
          )
        ]));
  }
}
