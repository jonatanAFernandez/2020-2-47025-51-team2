import 'package:bienestar/coreWidgets/ReservaItem.dart';

import 'consultar.reservas.conf.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../models/Reserva.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ConsultarReservas extends StatefulWidget {
  @override
  ConsultarReservasState createState() {
    return ConsultarReservasState();
  }
}

class ConsultarReservasState extends State<ConsultarReservas> {
  @override
  Widget build(BuildContext context) {
    debugPrint('ConsultarEspacios listView created');
    return _buildBody(context);
  }
}

Widget _buildBody(BuildContext context) {
  //el StreamBuilder chequea si los snapshots de la coleccion
  //tienen datos. Si sí tienen datos,entonces construye una lista a partir de
  //sus *documentos*

  return StreamBuilder<QuerySnapshot>(
    stream: FirebaseFirestore.instance.collection('reservas').snapshots(),
    builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
      if (snapshot.hasError) {
        throw snapshot.error;
      }

      if (snapshot.connectionState == ConnectionState.waiting) {
        return LinearProgressIndicator();
      }

      return _buildList(context, snapshot.data.docs);
    },
  );
}

Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
  return ListView(
    padding: const EdgeInsets.only(top: 20.0),
    children: snapshot.map((data) => _buildListItem(context, data)).toList(),
  );
}

Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
  //este es el widget de cada elemento individual,
  final reserva = Reserva.fromSnapshot(data);

  return Padding(
      key: ValueKey(reserva.elementoReservado),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ReservaItem(reserva, roleOptions: AdminOptionsReservas(reserva)));
}
