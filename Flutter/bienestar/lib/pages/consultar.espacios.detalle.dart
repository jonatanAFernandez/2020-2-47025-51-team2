import 'package:flutter/material.dart';
import '../models/Espacio.dart';
import '../models/Subelemento.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'dart:convert';

class VistaDetalle extends StatefulWidget {
  final Espacio espacioDetalle;

  // In the constructor, require a Espacio/elemento
  VistaDetalle({Key key, @required this.espacioDetalle}) : super(key: key);

  @override
  _VistaDetalleState createState() => _VistaDetalleState();
}

class _VistaDetalleState extends State<VistaDetalle> {
  int indiceSubelementoActual = 0;
  int valor = 1;

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.espacioDetalle.nombre),
        ),
        body: SingleChildScrollView(child: Column(children: [vistaDetalle()])));
  }

  Widget vistaDetalle() {
    var listView;

    if (widget.espacioDetalle.subelementos != null) {
      listView = espacioDetalleConElementos();
    } else {
      listView = espacioDetalleSinSubelementos();
    }

    return listView;
  }

  Widget espacioDetalleConElementos() {
    return Column(children: [
      Align(
        //Los ListTiles
        alignment: Alignment.center,
        child: Column(children: [
          ListTile(
              title: Text(widget.espacioDetalle.nombre,
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Nombre",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text(widget.espacioDetalle.descripcion,
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Descripción",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text(widget.espacioDetalle.recomendaciones,
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Recomendaciones",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text(widget.espacioDetalle.capacidadMaxima.toString(),
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Capacidad Máxima",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text(widget.espacioDetalle.unidades.toString(),
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Unidades",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text("Subelementos", style: TextStyle(fontSize: 20)),
              subtitle: Text("Subelementos disponibles",
                  style: TextStyle(fontSize: 15, color: Colors.grey)),
              trailing: botonFormulario()),
          // muestra los elementos si los hay en radiobutton
          SizedBox(
              height: 200.0,
              child: ListView.builder(
                  itemCount: widget.espacioDetalle.subelementos.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      onChanged: (int e) => indiceSubelemento(e, index),
                      value: index,
                      groupValue: indiceSubelementoActual,
                      title: Text(
                          widget.espacioDetalle.subelementos[index].nombre,
                          style: TextStyle(fontSize: 20)),
                      subtitle: Text(
                          widget.espacioDetalle.subelementos[index].descripcion,
                          style: TextStyle(fontSize: 15)),
                    );
                  })),
          Expanded(
              child: FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Colors.black,
                  padding: EdgeInsets.symmetric(horizontal: 125.0),
                  splashColor: Colors.blueAccent,
                  onPressed: () {},
                  child: Text(
                    "Reservar",
                    style: TextStyle(fontSize: 20.0),
                  ))),
        ]),
      ),
    ]);
  }

  Widget espacioDetalleSinSubelementos() {
    return Column(children: [
      Align(
        alignment: Alignment.center,
        child: Column(children: [
          ListTile(
              title: Text(widget.espacioDetalle.nombre,
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Nombre",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text(widget.espacioDetalle.descripcion,
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Descripción",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text(widget.espacioDetalle.recomendaciones,
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Recomendaciones",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text(widget.espacioDetalle.capacidadMaxima.toString(),
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Capacidad Máxima",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text(widget.espacioDetalle.unidades.toString(),
                  style: TextStyle(fontSize: 20)),
              subtitle: Text("Unidades",
                  style: TextStyle(fontSize: 15, color: Colors.grey))),
          ListTile(
              title: Text("Subelementos", style: TextStyle(fontSize: 20)),
              subtitle: Text("Subelementos disponibles",
                  style: TextStyle(fontSize: 15, color: Colors.grey)),
              trailing: botonFormularioSinSubelementos()),
          SizedBox(
              height: 100.0,
              child: ListView.builder(
                  itemCount: 1,
                  itemBuilder: (context, index) {
                    return ListTile(
                        title: Text("Sin subelementos",
                            style: TextStyle(fontSize: 20)),
                        subtitle: Text("Sin subelementos por mostrar",
                            style: TextStyle(fontSize: 15)));
                  })),
          Container(
              height: 60.0,
              child: ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: 1,
                  itemBuilder: (context, index) {
                    return ListTile(
                        title: Text(""),
                        subtitle: Text(""),
                        trailing: FlatButton(
                            color: Colors.blue,
                            textColor: Colors.white,
                            disabledColor: Colors.grey,
                            disabledTextColor: Colors.black,
                            padding: EdgeInsets.symmetric(horizontal: 125.0),
                            splashColor: Colors.blueAccent,
                            onPressed: () {
                              // var nuevaReserva =
                              //     Reserva(widget.espacioDetalle, 'admin');
                              // ReservasModel.items.add(nuevaReserva);
                              // Scaffold.of(context).showSnackBar(SnackBar(
                              //     content: Text(jsonEncode(nuevaReserva))));
                            },
                            child: Text(
                              "Reservar",
                              style: TextStyle(fontSize: 20.0),
                            )));
                  }))
        ]),
      ),
    ]);
  }

  void indiceSubelemento(int e, int index) {
    setState(() {
      if (e == index) {
        indiceSubelementoActual = index;
      }
    });
  }

  crearFormularioAnadir(BuildContext context) {
    TextEditingController controladorNombre = new TextEditingController();
    TextEditingController controladorDescripcion = new TextEditingController();

    Widget guardarButton = FlatButton(
      child: Text("Guardar"),
      onPressed: () {
        if (widget.espacioDetalle.subelementos == null) {
          Subelemento subelementoReserva = Subelemento(
              controladorNombre.text, controladorDescripcion.text, true);
          List<Subelemento> subelementoGuardar = [];
          subelementoGuardar.add(subelementoReserva);
          widget.espacioDetalle.subelementos = subelementoGuardar;
          setState(() {});
          Navigator.pop(context);
        } else {
          Subelemento subelementoReserva = Subelemento(
              controladorNombre.text, controladorDescripcion.text, true);
          widget.espacioDetalle.subelementos.add(subelementoReserva);
          setState(() {});
          Navigator.pop(context);
        }
      },
    );
    Widget cancelarButton = FlatButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Añadir nuevo subelemento"),
            content: Form(
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              TextFormField(
                controller: controladorNombre,
                decoration: InputDecoration(hintText: "Nombre"),
              ),
              SizedBox(height: 20),
              TextFormField(
                controller: controladorDescripcion,
                decoration: InputDecoration(hintText: "Descripcion"),
              )
            ])),
            actions: [guardarButton, cancelarButton],
          );
        });
  }

  crearFormularioEditar(BuildContext context) {
    TextEditingController controladorNombre = new TextEditingController();
    TextEditingController controladorDescripcion = new TextEditingController();

    Widget guardarButton = FlatButton(
      child: Text("Guardar"),
      onPressed: () {
        if (widget.espacioDetalle.subelementos == null) {
          widget.espacioDetalle.subelementos[indiceSubelementoActual]
              .setNombre = controladorNombre.text;
          widget.espacioDetalle.subelementos[indiceSubelementoActual]
              .setDescripcion = controladorDescripcion.text;
          setState(() {});
          Navigator.pop(context);
        } else {
          widget.espacioDetalle.subelementos[indiceSubelementoActual]
              .setNombre = controladorNombre.text;
          widget.espacioDetalle.subelementos[indiceSubelementoActual]
              .setDescripcion = controladorDescripcion.text;
          setState(() {});
          Navigator.pop(context);
        }
      },
    );
    Widget cancelarButton = FlatButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Editar subelemento"),
            content: Form(
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              TextFormField(
                controller: controladorNombre,
                decoration: InputDecoration(
                    hintText: widget.espacioDetalle
                        .subelementos[indiceSubelementoActual].nombre
                        .toString()),
              ),
              SizedBox(height: 20),
              TextFormField(
                controller: controladorDescripcion,
                decoration: InputDecoration(
                    hintText: widget.espacioDetalle
                        .subelementos[indiceSubelementoActual].descripcion
                        .toString()),
              )
            ])),
            actions: [guardarButton, cancelarButton],
          );
        });
  }

  Widget botonFormulario() {
    return DropdownButton(
        value: valor,
        items: [
          DropdownMenuItem(
            child: Text("Añadir"),
            value: 1,
          ),
          DropdownMenuItem(
            child: Text("Editar"),
            value: 2,
          ),
        ],
        onChanged: (value) {
          setState(() {
            valor = value;
            if (value == 1) {
              crearFormularioAnadir(context);
            } else {
              crearFormularioEditar(context);
            }
          });
        });
  }

  Widget botonFormularioSinSubelementos() {
    return DropdownButton(
        value: valor,
        items: [
          DropdownMenuItem(
            child: Text("Añadir"),
            value: 1,
          ),
        ],
        onChanged: (value) {
          setState(() {
            valor = value;
            crearFormularioAnadir(context);
          });
        });
  }
}
