import 'dart:convert';
import 'package:bienestar/Modulos/ModuloHorario.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../models/Espacio.dart';
import '../models/DisponibilidadHoraria.dart';
import '../models/EspacioFormControllers.dart';
import '../coreWidgets/ControllerFormField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

//Este formulario recibe los datos necesarios para crear un Espacio.

class CrearEspacioForm extends StatefulWidget {
  @override
  CrearEspacioFormState createState() {
    return CrearEspacioFormState();
  }
}

class CrearEspacioFormState extends State<CrearEspacioForm> {
  var _formKey = GlobalKey<FormState>();
  var tipoSelected = 'Espacio';
  var tiposDeElemento = ['Espacio', 'Elemento', 'Elemento de un espacio'];
  final firestore = FirebaseFirestore.instance;

  EspacioFormControllers campos = EspacioFormControllers();
  ModuloHorario horario = ModuloHorario();

  /// Añado un nuevo [Espacio] a la colección 'espacios'
  Future<void> fsAnadirEspacio(Espacio espacio) {
    return firestore
        .collection('espacios')
        .add(espacio.toJson())
        .then((value) => debugPrint("Wrote to Firestore: ${espacio.toJson()}"));
  }

  // Validators

  Function validarCampoVacio = (String value) {
    if (value.isEmpty) {
      return 'Este campo no puede estar vacío';
    }
    return null;
  };

  @override
  Widget build(BuildContext context) {
    debugPrint('Crear-espacio view created');

    return ListView(children: [
      Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(10),
                alignment: Alignment.bottomLeft,
                child: Text(
                  'Información Básica',
                  style: TextStyle(
                      fontFamily: "Raleway",
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                )),
            // Campos

            Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                alignment: Alignment.bottomLeft,
                child: Column(
                  children: [
                    ControllerFormField(
                        'Nombre', campos.nombre, validarCampoVacio),
                    ControllerFormField(
                        'Descripción', campos.descripcion, validarCampoVacio),
                    ControllerFormField('Recomendaciones de uso',
                        campos.recomendaciones, validarCampoVacio),
                    ControllerFormField('Capacidad máxima',
                        campos.capacidadMaxima, validarCampoVacio),
                    ControllerFormField('Número de unidades', campos.unidades,
                        validarCampoVacio),
                  ],
                )),

            // Horario

            horario.darWidget(),

            Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: RaisedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      // Preparar datos para instanciar un Espacio

                      String nombre = campos.nombre.text;
                      String descripcion = campos.descripcion.text;
                      int capacidad = int.parse(campos.capacidadMaxima.text);
                      String recomendaciones = campos.recomendaciones.text;
                      int unidades = int.parse(campos.unidades.text);

                      DisponibilidadHoraria horarioNuevo =
                          this.horario.construirHorario();

                      var espacioNuevo = Espacio(nombre, descripcion,
                          recomendaciones, capacidad, unidades, horarioNuevo);

                      debugPrint('Attempted to write to firestore using add()');

                      Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text(jsonEncode(espacioNuevo))));

                      return fsAnadirEspacio(espacioNuevo);
                    }
                  },
                  child: Text('Crear espacio o elemento'),
                )),
          ],
        ),
      )
    ]);
  }
}
