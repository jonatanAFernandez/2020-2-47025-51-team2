import 'package:bienestar/Modulos/ModuloHorario.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../models/Espacio.dart';
import '../models/DisponibilidadHoraria.dart';
import '../models/EspacioFormControllers.dart';
import '../coreWidgets/ControllerFormField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class EditarEspacioForm extends StatefulWidget {
  //básicamente para almacenar la referencia
  final Espacio espacio;

  EditarEspacioForm(this.espacio);

  // Aquí encapsulo el procedimiento en Firestore
  // Modificar solo aquí:
  Future<void> firestoreUpdateData(Espacio espacio) {
    return espacio.reference
        .update(espacio.toJson())
        .then((value) => debugPrint('Update completed'));
  }

  @override
  EditarEspacioFormState createState() {
    return EditarEspacioFormState();
  }
}

class EditarEspacioFormState extends State<EditarEspacioForm> {
  var _formKey = GlobalKey<FormState>();
  final firestore = FirebaseFirestore.instance;
  Espacio espacio;

  EspacioFormControllers campos = EspacioFormControllers();
  ModuloHorario horario = ModuloHorario();
  // Validators
  // ignore: top_level_function_literal_block
  var validarCampoVacio = (String value) {
    if (value.isEmpty) {
      return 'Este campo no puede estar vacío';
    }
    return null;
  };

  @override
  Widget build(BuildContext context) {
    debugPrint('Editar-espacio view created');
    // setState(() {
    //   nombreController = TextEditingController.fromValue(
    //       TextEditingValue.fromJSON(widget.espacio.toJson()));
    // });
    setState(() {
      espacio = widget.espacio;
    });

    return Scaffold(
        appBar: AppBar(
          title: Text("Editar Espacio"),
        ),
        body: ListView(children: [
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                ControllerFormField(
                    widget.espacio.nombre, campos.nombre, validarCampoVacio),
                ControllerFormField(widget.espacio.descripcion,
                    campos.descripcion, validarCampoVacio),
                ControllerFormField(widget.espacio.recomendaciones,
                    campos.recomendaciones, validarCampoVacio),
                ControllerFormField("${widget.espacio.capacidadMaxima}",
                    campos.capacidadMaxima, validarCampoVacio),
                ControllerFormField("${widget.espacio.unidades}",
                    campos.unidades, validarCampoVacio),
                horario.darEditableWidget(widget.espacio.horario),
                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          // Preparar datos para instanciar un Espacio
                          String nombre;
                          String descripcion;
                          int capacidad;
                          String recomendaciones;
                          int unidades;
                          DisponibilidadHoraria horario;

                          campos.nombre.text != null
                              ? nombre = campos.nombre.text
                              : nombre = espacio.nombre;
                          campos.descripcion.text != null
                              ? descripcion = campos.descripcion.text
                              : descripcion = espacio.descripcion;
                          campos.capacidadMaxima.text != null
                              ? capacidad =
                                  int.parse(campos.capacidadMaxima.text)
                              : capacidad = espacio.capacidadMaxima;
                          campos.recomendaciones.text != null
                              ? recomendaciones = campos.recomendaciones.text
                              : recomendaciones = espacio.recomendaciones;
                          campos.unidades.text != null
                              ? unidades = int.parse(campos.unidades.text)
                              : unidades = espacio.unidades;

                          horario = this.horario.construirHorario();

                          espacio.setNombre(nombre);
                          espacio.setDescripcion(descripcion);
                          espacio.setCapacidadMaxima(capacidad);
                          espacio.setRecomendaciones(recomendaciones);
                          espacio.setUnidades(unidades);
                          espacio.setHorario(horario);

                          // String log = jsonEncode(espacioNuevo);

                          debugPrint('Attempted update document:');
                          debugPrint(espacio.toJson()['nombre'],
                              wrapWidth: 1000);

                          // Scaffold.of(context)
                          //     .showSnackBar(SnackBar(content: Text('Updated')));
                          return widget.firestoreUpdateData(espacio);
                        }
                      },
                      child: Text('Modificar espacio o elemento'),
                    )),
              ],
            ),
          )
        ]));
  }
}
