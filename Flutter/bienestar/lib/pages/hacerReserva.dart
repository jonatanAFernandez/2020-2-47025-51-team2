import 'package:bienestar/Modulos/ModuloVisualizarHorario.dart';
import 'package:bienestar/estilos_improvisados.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Modulos/ModuloInvitados.dart';
import '../models/Espacio.dart';
import '../models/Reserva.dart';
import '../models/Beneficiario.dart';
import '../Modulos/ModuloVisualizarHorario.dart';
import '../Modulos/firebaseOperations.dart';
import '../main.dart';

class HacerReserva extends StatefulWidget {
  final Espacio espacio;
  GlobalKey<ScaffoldState> _scaffoldDeHome;

  HacerReserva(this.espacio, this._scaffoldDeHome);
  @override
  State<StatefulWidget> createState() {
    return HacerReservaState(espacio);
  }
}

class HacerReservaState extends State<HacerReserva> {
  List<Beneficiario> listaInvitados;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Espacio espacio;
  DateTime hora;
  final debugPrepend = "[Modulo Agendamiento]";
  HacerReservaState(this.espacio) {
    this.listaInvitados = List<Beneficiario>();
    hora = darFechaActualMas15Minutos();
  }

  _debugPrint() {
    debugPrint("$debugPrepend lista invitados: $listaInvitados");
    debugPrint("$debugPrepend espacio $espacio");
    debugPrint("$debugPrepend hora: $hora");
  }

  @override
  Widget build(BuildContext context) {
    _debugPrint();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text('Haz una reserva')),
      body: Column(children: [
        ListTile(
            title: Text(
          "Disponibilidad",
          style: tit_seccion1,
        )),
        new ModuloVisualizarHorario().darTablaDisponibilidad(espacio.horario),
        ListTile(
            title: Text(
          "Hora Reserva",
          style: tit_seccion1,
        )),
        SizedBox(
          height: 70,
          child: CupertinoDatePicker(
              onDateTimeChanged: (date) {
                setState(() {
                  hora = date;
                  debugPrint("$debugPrepend usuario cambió hora");
                });
              },
              initialDateTime: darFechaActualMas15Minutos(),
              minimumDate: darFechaActualMas15Minutos(),
              maximumDate: darFechaActualMas1Mes()),
        ),
        ListTile(
            title: Text(
          "Invitados",
          style: tit_seccion1,
        )),
        ConstrainedBox(
          constraints: new BoxConstraints(minHeight: 175, maxHeight: 175),
          child: ModuloInvitados(
              listaInvitados, _scaffoldKey, espacio.capacidadMaxima),
        ),
        RaisedButton(
          child: Text("Reservar"),
          onPressed: () {
            if (validarCapacidad(
                listaInvitados.length, espacio.capacidadMaxima)) {
              var nuevaReserva = Reserva.conAgendamiento(
                  espacio, auth.currentUser.email, auth.currentUser.uid, hora);
              listaInvitados.forEach((element) {
                print(element.toString());
              });
              debugPrint((listaInvitados.length == 0).toString());

              procedimientoHacerReserva(nuevaReserva, listaInvitados);

              debugPrint("$debugPrepend lista invitados: $listaInvitados");
              debugPrint(listaInvitados.runtimeType.toString());

              var snackbar = SnackBar(
                  duration: Duration(seconds: 2),
                  content: Text(
                      "Haz hecho una reserva. Revísala en la pestáña 'Mi reserva'"));

              widget._scaffoldDeHome.currentState.showSnackBar(snackbar);
              Navigator.of(context).pop();
            } else {
              var snackbar = SnackBar(
                  duration: Duration(seconds: 5),
                  content: Text(
                      "Número de personas en el espacio supera su capacidad máxima"));
              _scaffoldKey.currentState.showSnackBar(snackbar);
            }
          },
        )
      ]),
    );
  }
}

DateTime darFechaActualMas15Minutos() {
  return new DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, DateTime.now().hour, DateTime.now().minute + 15);
}

DateTime darFechaActualMas1Mes() {
  return new DateTime(DateTime.now().year, DateTime.now().month + 1,
      DateTime.now().day, DateTime.now().hour, DateTime.now().minute);
}

/// La capacidad del espacio incluye al [Beneficiario] que reserva el espacio;
/// por eso, se tiene que sumar 1 a los invitados
validarCapacidad(int longitudListaInvitados, int capacidadEspacio) {
  if (longitudListaInvitados + 1 > capacidadEspacio) return false;

  return true;
}
