import 'package:flutter/material.dart';

const String fontRaleway = 'Raleway';
const String fontLato = 'Lato';

const Headline1 = TextStyle(
    fontFamily: fontRaleway,
    fontSize: 98,
    fontWeight: FontWeight.w300,
    letterSpacing: -1.5);
const Headline2 = TextStyle(
    fontFamily: fontRaleway,
    fontSize: 61,
    fontWeight: FontWeight.w300,
    letterSpacing: -0.5);

const Headline3 = TextStyle(
    fontFamily: fontRaleway, fontSize: 49, fontWeight: FontWeight.w400);

const Headline4 = TextStyle(
    fontFamily: fontRaleway,
    fontSize: 35,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.25);

const Headline5 = TextStyle(
    fontFamily: fontRaleway, fontSize: 24, fontWeight: FontWeight.w400);

const Headline6 = TextStyle(
    fontFamily: fontRaleway,
    fontSize: 19,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.15);

const Subtitle1 = TextStyle(
    fontFamily: fontRaleway,
    fontSize: 16,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.15);

const Subtitle2 = TextStyle(
    fontFamily: fontRaleway,
    fontSize: 14,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.1);

const BodyText1 = TextStyle(
    fontFamily: fontLato,
    fontSize: 17,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.5);

const BodyText2 = TextStyle(
    fontFamily: fontLato,
    fontSize: 15,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.25);

const Button = TextStyle(
    fontFamily: fontLato,
    fontSize: 15,
    fontWeight: FontWeight.w500,
    letterSpacing: 1.25);

const Caption = TextStyle(
    fontFamily: fontLato,
    fontSize: 13,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.4);

const Verline = TextStyle(
    fontFamily: fontLato,
    fontSize: 10,
    fontWeight: FontWeight.w400,
    letterSpacing: 1.5);
