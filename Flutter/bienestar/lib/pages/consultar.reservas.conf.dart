import 'package:bienestar/Modulos/ModuloBloqueChips.dart';
import 'package:bienestar/Modulos/ModuloReagendamiento.dart';
import 'package:flutter/material.dart';

class AdminOptionsReservas extends StatelessWidget {
  // Fila de opciones del administrador.
  // La fila de botones se ponen dentro de un ListTile
  // Recibe una reserva
  final _reserva;
  AdminOptionsReservas(this._reserva);

  @override
  Widget build(BuildContext context) {
    debugPrint("Admin options for 'Reserva' loaded");
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      AdminReservasDeleteButton(_reserva),
      // AdminReservasDetalleButton(EditarReservaForm(_reserva)),
      AdminReservasReagendarButton(_reserva),
      AdminReservasInvitadosButton(_reserva)
    ]);
  }
}

class AdminReservasDeleteButton extends RaisedButton {
  final _reserva;

  AdminReservasDeleteButton(this._reserva);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        onPressed: () {
          var log;
          try {
            log = _reserva.toJson();
          } catch (e) {
            print(e);
          }
          ;
          _reserva.reference.delete();

          try {
            debugPrint('deleted from firestore $log');
          } catch (e) {
            print(e);
          }
          ;
        },
        child: Text('Cancelar'));
  }
}

class AdminReservasDetalleButton extends RaisedButton {
  final _route;

  AdminReservasDetalleButton(this._route);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => _route),
          );
        },
        child: Text('Detalle'));
  }
}

///Al presionarlo, retorna un [AlertDialog] para reagendar la reserva
class AdminReservasReagendarButton extends RaisedButton {
  final reserva;

  AdminReservasReagendarButton(this.reserva);

  @override
  Widget build(BuildContext context) {
    debugPrint(
        'Hora Agendado de elemento seleccionado: ${reserva.horaAgendada}');
    return RaisedButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return ModuloReagendamiento(reserva);
              });

          // showDateRangePicker(
          //     context: context,
          //     firstDate: DateTime.now(),
          //     lastDate: DateTime(DateTime.now().year, DateTime.now().month + 1,
          //         DateTime.now().day))
          //         ;
        },
        child: Text('Reagendar'));
  }
}

/// Crea un [MaterialPage] con el [ModuloBloqueChips] customizado para invitados
class AdminReservasInvitadosButton extends RaisedButton {
  final reserva;
  AdminReservasInvitadosButton(this.reserva);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Scaffold(
                appBar: AppBar(title: Text("Edición")),
                body: ModuloBloqueChips(reserva.invitados, reserva)),
          ),
        );
      },
      child: Text('Invitados'),
    );
  }
}
