import 'package:bienestar/adminHome.dart';
import 'package:bienestar/auth.dart';
import 'package:bienestar/models/Beneficiario.dart';
// import 'package:bienestar/pages/dummy.data.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import './models/Espacio.dart' show Espacio;
import './models/Reserva.dart' show Reserva;
// import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import './auth.dart';
import './adminHome.dart';
import './BeneHome.dart';
import './pages/styles.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  return runApp(MyApp());
}

final FirebaseAuth auth = FirebaseAuth.instance;
Beneficiario beneficiarioLogueado;

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  // ignore: unused_field
  final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: ['email'],
  );

  @override
  Widget build(BuildContext context) {
    //Este FutureBuilder se utiliza para inicializar FlutterFire
    // Tiene que hacerse antes de llamar métodos de autenticación
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return Text('el snapshot.hasError', textDirection: TextDirection.ltr);
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          // inicializo la autenticación

          auth.authStateChanges().listen((User user) {
            if (user == null) {
              print('[FirebaseAuth init] User is currently signed out!');
            } else {
              print('[FirebaseAuth init] User is signed in!');
            }
          });
          // FirebaseAuth.instance.signOut();

          return MaterialApp(
            title: 'Bienestar',
            theme: ThemeData(
              brightness: Brightness.light,

              // primaryColor: Color(0xffC1272D),
              primaryColor: Color(0xff1FD2C9),
              accentColor: Color(0xffd21f28),
              buttonColor: Color(0xffee4a52),
              textTheme: TextTheme(
                  title: Headline6,
                  // headline: Headline6,
                  body1: BodyText2,
                  button: Button,
                  subtitle: Subtitle1),
            ),
            home: Scaffold(
                appBar: AppBar(title: Text("Iniciar Sesión")),
                body: LoginPage(),
                resizeToAvoidBottomPadding: false),
            routes: <String, WidgetBuilder>{
              '/admin': (BuildContext context) => AdminHome(),
              '/bene': (BuildContext context) => BeneHome(),
              '/login': (BuildContext context) => LoginPage()
            },
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return Text('cargando...', textDirection: TextDirection.ltr);
      },
    );
  }
}

// los comentrios dentro de este ChangeNotifier son de la documentacion.
// Solo es una guía.
class EspaciosModel extends ChangeNotifier {
  /// Internal, private state of the cart.
  List<Espacio> _items;
  get items => _items;

  /// Adds [item] to cart. This and [removeAll] are the only ways to modify the
  /// cart from the outside.

  int length;
  EspaciosModel(List<Espacio> initialData) {
    this._items = initialData;
    this.length = this._items.length;
  }
  setLength() {
    this.length = this._items.length;
  }

  void add(Espacio nuevo) {
    _items.add(nuevo);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
    setLength();
  }

  /// Removes all items from the cart.
  void removeAll() {
    _items.clear();
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
    setLength();
  }

  Map toJson() => {
        'count': length,
        'espacios': items,
      };
}

class ReservasModel extends ChangeNotifier {
  static List<Reserva> items = [];
  int length;

  ReservasModel(List<Reserva> initialData) {
    ReservasModel.items = initialData;
    this.length = ReservasModel.items.length;
  }

  setLength() {
    this.length = ReservasModel.items.length;
  }

  void add(Reserva nuevo) {
    items.add(nuevo);

    notifyListeners();
    setLength();
  }

  void removeAll() {
    items.clear();

    notifyListeners();
    setLength();
  }

  Map toJson() => {
        'count': length,
        'reservas': items,
      };
}
