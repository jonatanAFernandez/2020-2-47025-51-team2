// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'auth.dart';
import 'pages/crear.espacio.dart';
import './pages/consultar.espacios.dart';
import './pages/consultar.reserva.dart';
import './main.dart';

class AdminHome extends StatefulWidget {
// La clase Home tiene como funcón principal mostrar otros widgets en el navigationBar
// Por ello guarda una lista de widgets que desplegar

  AdminHome({Key key}) : super(key: key);

  @override
  _AdminHomeState createState() => _AdminHomeState();
}

class _AdminHomeState extends State<AdminHome> {
  var _selectedIndex = 0;
  final _adminScaffoldKey = GlobalKey<ScaffoldState>();
//Lista de widgets que seleccionar desde el bottomNavigationBar

  MaterialPageRoute login = MaterialPageRoute(
    builder: (context) => Scaffold(
      appBar: AppBar(title: Text("Iniciar Sesión")),
      body: LoginPage(),
      resizeToAvoidBottomPadding: false,
    ),
  );

  @override
  Widget build(BuildContext context) {
    // loadDummyToFirestore();
    List<Widget> _widgetOptions = <Widget>[
      CrearEspacioForm(),
      ConsultarEspacios(_adminScaffoldKey),
      ConsultarReservas()
    ];
    var logout = Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      alignment: Alignment.center,
      child: GestureDetector(
          onTap: () {
            auth.signOut();
            Navigator.pushReplacement(context, login);
          },
          child: Text(
            "Sign out",
            style: TextStyle(decoration: TextDecoration.underline),
          )),
    );

    var usuario = Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        alignment: Alignment.center,
        child: Text(
          "Admin",
        ));

    return Scaffold(
      key: _adminScaffoldKey,
      appBar: AppBar(
        title: Text('Dashboard'),
        actions: [usuario, logout],
      ),
      body: _widgetOptions[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.add), title: Text('Crear Espacio')),
          BottomNavigationBarItem(
              icon: Icon(Icons.list), title: Text('Consultar')),
          BottomNavigationBarItem(
              icon: Icon(Icons.schedule), title: Text('Reseva')),
        ],
        currentIndex: _selectedIndex,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
            debugPrint("Tab index has been changet to: $_selectedIndex");
          });
        },
      ),
    );
  }
}
