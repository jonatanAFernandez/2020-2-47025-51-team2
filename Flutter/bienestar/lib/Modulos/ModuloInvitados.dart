import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../models/Beneficiario.dart';

/// Toma un Lista y a partir de ella construye una [listaEnMemoria] y muestra  [Chips] que puede adicionar o eliminar
class ModuloInvitados extends StatefulWidget {
  List<Beneficiario> listaInicial;
  int capacidadMaxima;
  GlobalKey<ScaffoldState> _scaffoldKey;

  ModuloInvitados(this.listaInicial, this._scaffoldKey, this.capacidadMaxima);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return BloqueChipsState(listaInicial);
  }
}

class BloqueChipsState extends State<ModuloInvitados> {
  //Cambiará su valor con setState
  List<Beneficiario> listaEnMemoria;

  BloqueChipsState(this.listaEnMemoria);

  @override
  Widget build(BuildContext context) {
    var list = listaEnMemoria
        .map((elementoEnLista) => Chip(
            label: Text("${elementoEnLista.toString()}"),
            onDeleted: () {
              setState(() {
                listaEnMemoria.remove(elementoEnLista);
                debugPrint("Element removed");
              });
            }))
        .toList();

    return Column(children: [
      ListTile(
        trailing: FlatButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                    scrollable: true,
                    content: Container(
                      height: 200.0,
                      width: 400.0,
                      child: StreamBuilder<QuerySnapshot>(
                        stream: FirebaseFirestore.instance
                            .collection('beneficiarios')
                            .snapshots(),
                        builder:
                            (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (!snapshot.hasData)
                            return LinearProgressIndicator();

                          return ListView(
                            children: snapshot.data.docs
                                .map(
                                  (data) => InputChip(
                                      label: Text(
                                          Beneficiario.fromSnapshot(data)
                                              .toString()),
                                      onPressed: () {
                                        setState(() {
                                          listaEnMemoria.add(
                                              Beneficiario.fromSnapshot(data));
                                        });

                                        var snackbar = SnackBar(
                                            duration: Duration(seconds: 5),
                                            content: Text(
                                                "Se añadió un invitado a la reserva en curso"));
                                        widget._scaffoldKey.currentState
                                            .showSnackBar(snackbar);
                                        Navigator.of(context).pop();
                                      }),
                                )
                                .toList(),
                          );
                        },
                      ),
                    ),
                    title: Text("Ingresar"),
                    actions: [
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text('Cancelar'))
                    ]),
              );
            },
            child: Icon(Icons.add)),
      ),
      ListTile(
          title: Text("Capacidad:"),
          trailing:
              Text("${listaEnMemoria.length + 1} / ${widget.capacidadMaxima}")),
      listaEnMemoria == null || listaEnMemoria.length == 0
          ? Text("Aún no has añadido invitados")
          : SizedBox(child: ListView(children: list), height: 50)
    ]);
  }
}
