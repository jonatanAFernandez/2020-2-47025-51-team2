import 'package:bienestar/models/DisponibilidadHoraria.dart';

import 'package:flutter/material.dart';

/// Muestra un objeto [DisponibilidadHoraria] en una tabla
class ModuloVisualizarHorario {
  Widget darTablaDisponibilidad(DisponibilidadHoraria horario) {
    return Table(children: [
      TableRow(children: [
        Column(children: [_header("L")]),
        Column(children: [_rango(horario.dias["lunes"])]),
      ]),
      TableRow(children: [
        Column(children: [_header("M")]),
        Column(children: [_rango(horario.dias["martes"])]),
      ]),
      TableRow(children: [
        Column(children: [_header("M")]),
        Column(children: [_rango(horario.dias["miercoles"])]),
      ]),
      TableRow(children: [
        Column(children: [_header("J")]),
        Column(children: [_rango(horario.dias["jueves"])]),
      ]),
      TableRow(children: [
        Column(children: [_header("V")]),
        Column(children: [_rango(horario.dias["viernes"])]),
      ]),
      TableRow(children: [
        Column(children: [_header("S")]),
        Column(children: [_rango(horario.dias["sabado"])]),
      ]),
    ]);
  }
}

_header(String head) {
  return Text(
    head,
    textAlign: TextAlign.left,
    style: TextStyle(fontWeight: FontWeight.bold),
  );
}

_rango(dia) {
  return Text("${dia["inicio"]} - ${dia["fin"]} ");
}
