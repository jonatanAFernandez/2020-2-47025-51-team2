import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Toma un Lista y a partir de ella construye una [listaEnMemoria] y muestra  [Chips] que puede adicionar o eleminar
class ModuloBloqueChips<T> extends StatefulWidget {
  final elemento;
  final List<T> listaInicial;

  ModuloBloqueChips(this.listaInicial, this.elemento);
  @override
  State<StatefulWidget> createState() {
    return BloqueChipsState(listaInicial);
  }
}

class BloqueChipsState<T> extends State<ModuloBloqueChips> {
  //Cambiará su valor con setState
  List<T> listaEnMemoria;

  BloqueChipsState(this.listaEnMemoria);

  @override
  Widget build(BuildContext context) {
    debugPrint(
        "[Modulo Bloque Chips] tipo de lista inicializada: ${listaEnMemoria.runtimeType}");
    var list = listaEnMemoria
        .map((elementoEnLista) => Chip(
            label: Text("${elementoEnLista.toString()}"),
            onDeleted: () {
              setState(() {
                listaEnMemoria.remove(elementoEnLista);
                debugPrint("Element removed");
              });
              widget.elemento.invitados = listaEnMemoria;
              widget.elemento.reference
                  .updateData(widget.elemento.toJson())
                  .then((value) {
                debugPrint('Update completed');

                Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text(
                        'Se removió invitado en la reserva ${widget.elemento.toString()}')));
              });
            }))
        .toList();

    return Column(children: [
      ListTile(
        trailing: FlatButton(
            onPressed: () {
              debugPrint('[ModuloBloqueChips] impleméntame');
            },
            child: Icon(Icons.add)),
      ),
      list.length > 0
          ? Column(children: list)
          : Text("Esta reserva no tiene invitados"),
    ]);
  }
}
