import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bienestar/models/Reserva.dart';

class ModuloReagendamiento extends StatefulWidget {
  Reserva reserva;
  DateTime fechaInicial;
  ModuloReagendamiento(this.reserva) {
    fechaInicial = reserva.horaAgendada;
    debugPrint(
        "[Modulo de Reagendamiento] cargó en módulo: ${reserva.toString()}");
  }

  @override
  State<StatefulWidget> createState() {
    return ModuloReagendamientoState(fechaInicial);
  }
}

class ModuloReagendamientoState extends State<ModuloReagendamiento> {
  DateTime fecha;
  ModuloReagendamientoState(this.fecha);

  @override
  Widget build(BuildContext context) {
    debugPrint("[Modulo Reagendamiento]fecha de reserva seleccionada: $fecha");
    return AlertDialog(
        content: SingleChildScrollView(
            child: SizedBox(
          height: 70,
          child: CupertinoDatePicker(
            onDateTimeChanged: (date) {
              fecha = date;
              print("[Modulo Reagendamiento] Cambio de fecha a $date");
            },
            initialDateTime: fecha,
          ),
        )),
        title: Text("Reagendar Reserva"),
        actions: [
          FlatButton(
              onPressed: () {
                widget.reserva.setHoraAgendada(fecha);
                debugPrint(
                    "[Modulo Horario] Agendamiento antes de update: ${widget.reserva.horaAgendada.toString()}");
                widget.reserva.reference
                    .update(widget.reserva.toJson())
                    .then((value) => debugPrint('Update completed'));

                Navigator.of(context).pop();
              },
              child: Text('Confirmar')),
          FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Cancelar'))
        ]);
  }
}
