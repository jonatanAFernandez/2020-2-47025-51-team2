import 'package:flutter/material.dart';
import '../models/Espacio.dart';

/// Permite añadir y eliminar los subelementos de un [Espacio].
/// Tan solo modifica el estado del Espacio.
class ModuloSubelementos {
  final Espacio espacio;
  ModuloSubelementos(this.espacio);

  Widget darWidgetAdministrador(BuildContext context) {
    return SizedBox(
        height: 200.0,
        child: ListView.builder(
            itemCount: espacio.subelementos.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(espacio.subelementos[index].nombre,
                    style: TextStyle(fontSize: 20)),
                subtitle: Text(espacio.subelementos[index].descripcion,
                    style: TextStyle(fontSize: 15)),
              );
            }));
  }
}
