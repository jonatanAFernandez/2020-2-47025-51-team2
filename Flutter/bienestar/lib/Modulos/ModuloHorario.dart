import '../coreWidgets/BloqueHorario.dart';
import '../coreWidgets/BloqueHorarioEditable.dart';
import 'package:bienestar/models/DisponibilidadHoraria.dart';
import '../models/DisponibilidadPorDia.dart';
import '../models/ControladoresBloqueHorario.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

/// Agrupa [ControladoresBloqueHorario] y [BloqueHorario] para crear
/// un Widget que gestione [DisponibilidadHoraria], a partir de información
/// ingresada en formularios
class ModuloHorario {
  ControladoresBloqueHorario lunes;
  ControladoresBloqueHorario martes;
  ControladoresBloqueHorario miercoles;
  ControladoresBloqueHorario jueves;
  ControladoresBloqueHorario viernes;
  ControladoresBloqueHorario sabado;

  Map<String, ControladoresBloqueHorario> controllers;

  Map<String, dynamic> toJson() => {
        "lunes": controllers["lunes"].toJson(),
        "martes": controllers["martes"].toJson(),
        "miercoles": controllers["miercoles"].toJson(),
        "jueves": controllers["jueves"].toJson(),
        "viernes": controllers["viernes"].toJson(),
        "sabado": controllers["sabado"].toJson()
      };

//Lee la información de los controladores y retorno un objeto de tipo
//DisponibilidadHoraria

  ModuloHorario() {
    lunes = ControladoresBloqueHorario();
    martes = ControladoresBloqueHorario();
    miercoles = ControladoresBloqueHorario();
    jueves = ControladoresBloqueHorario();
    viernes = ControladoresBloqueHorario();
    sabado = ControladoresBloqueHorario();

    controllers = {
      "lunes": lunes,
      "martes": martes,
      "miercoles": miercoles,
      "jueves": jueves,
      "viernes": viernes,
      "sabado": sabado
    };
  }

  ///Lee el estado de los controladores de [ModuloHorario] y retorna un
  /// nuevo objeto [DisponibilidadHoraria] a partir de ellos.
  DisponibilidadHoraria construirHorario() {
    DisponibilidadPorDia lunes =
        DisponibilidadPorDia.fromControladores(controllers['lunes']);
    DisponibilidadPorDia martes =
        DisponibilidadPorDia.fromControladores(controllers['martes']);
    DisponibilidadPorDia miercoles =
        DisponibilidadPorDia.fromControladores(controllers['miercoles']);
    DisponibilidadPorDia jueves =
        DisponibilidadPorDia.fromControladores(controllers['jueves']);
    DisponibilidadPorDia viernes =
        DisponibilidadPorDia.fromControladores(controllers['viernes']);
    DisponibilidadPorDia sabado =
        DisponibilidadPorDia.fromControladores(controllers['sabado']);

    debugPrint("Horario controllers returned ${jsonEncode(controllers)}");
    DisponibilidadHoraria horario = DisponibilidadHoraria(
        lunes, martes, miercoles, jueves, viernes, sabado);

    return horario;
  }

  Function validarCampoVacio = (String value) {
    if (value.isEmpty) {
      return 'Este campo no puede estar vacío';
    }
    return null;
  };

  Widget darWidget() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(children: [
          // ListTile(title: Text('Disponibilidad Horaria')),

          Padding(padding: EdgeInsets.all(15)),
          Container(
              child: Text(
                'Disponibilidad Horaria',
                style: TextStyle(
                    fontFamily: "Raleway",
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              alignment: Alignment.bottomLeft),

          BloqueHorario('Lunes', controllers["lunes"].inicio,
              controllers["lunes"].fin, validarCampoVacio),
          BloqueHorario('Martes', controllers["martes"].inicio,
              controllers["martes"].fin, validarCampoVacio),
          BloqueHorario('Miércoles', controllers["miercoles"].inicio,
              controllers["miercoles"].fin, validarCampoVacio),
          BloqueHorario('Jueves', controllers["jueves"].inicio,
              controllers["jueves"].fin, validarCampoVacio),
          BloqueHorario('Viernes', controllers["viernes"].inicio,
              controllers["viernes"].fin, validarCampoVacio),
          BloqueHorario('Sabado', controllers["sabado"].inicio,
              controllers["sabado"].fin, validarCampoVacio),
        ]));
  }

  Widget darEditableWidget(DisponibilidadHoraria horario) {
    return Column(children: [
      ListTile(title: Text('Disponibilidad Horaria')),
      BloqueHorarioEditable.fromDisponibilidad(
          horario,
          "lunes",
          controllers['lunes'].inicio,
          controllers['lunes'].fin,
          validarCampoVacio),
      BloqueHorarioEditable.fromDisponibilidad(
          horario,
          "martes",
          controllers['martes'].inicio,
          controllers['martes'].fin,
          validarCampoVacio),
      BloqueHorarioEditable.fromDisponibilidad(
          horario,
          "miercoles",
          controllers['miercoles'].inicio,
          controllers['miercoles'].fin,
          validarCampoVacio),
      BloqueHorarioEditable.fromDisponibilidad(
          horario,
          "jueves",
          controllers['jueves'].inicio,
          controllers['jueves'].fin,
          validarCampoVacio),
      BloqueHorarioEditable.fromDisponibilidad(
          horario,
          "viernes",
          controllers['viernes'].inicio,
          controllers['viernes'].fin,
          validarCampoVacio),
      BloqueHorarioEditable.fromDisponibilidad(
          horario,
          "sabado",
          controllers['sabado'].inicio,
          controllers['sabado'].fin,
          validarCampoVacio),
    ]);
  }

  Widget darWidgetReagendar() {
    // TODO: [Felipe]. Aquí debera retornar el widget que reagende una reserva
    // reagendar implica primero visualizar la hora de la reserva y luego
    // dar la opción de cambiarla.  Deberá valerse de las clases ya definidas
    // revise [BloqueHorario] y [BloqueHorarioEditable].
    return throw UnimplementedError();
  }
}
