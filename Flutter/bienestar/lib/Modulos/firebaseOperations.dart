import '../models/Reserva.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../main.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../models/Beneficiario.dart';

// ignore: non_constant_identifier_names
final db_string = "[Firebase Operations]";

/// Hace una reserva y se encarga de gestionar la referenciación
/// involucrada en ello
procedimientoHacerReserva(
    Reserva reservaAGuardar, List<Beneficiario> arregloInvitados) async {
  var fireBase = FirebaseFirestore.instance;
  // ** Legacy code en caso de que mi sistema fracase**

  // Obtengo mi usuario a partir del auth.CurrentUser
  // QuerySnapshot queryBeneficiario = await fireBase
  //     .collection("beneficiarios")
  //     .where("email", isEqualTo: auth.currentUser.email)
  //     .get();

  // DocumentReference referenciaBeneficiario =
  //     queryBeneficiario.docs.first.reference;
  //** Fin del legacy code */

  // escribo en la colección 'reservas'
  DocumentReference referenciaReserva =
      await fireBase.collection('reservas').add(reservaAGuardar.toJson());
  // adjunto a los invitados
  List arregloInvitadosJson = [];

  arregloInvitados.forEach((element) {
    arregloInvitadosJson.add(element.toJson());
    // debugPrint("$db_string se encontró $element en el arreglo");
  });
  debugPrint(
      "$db_string para escribir en firestore ${arregloInvitadosJson.toString()}");
  fireBase
      .collection("reservas")
      .doc(referenciaReserva.id)
      .update({"invitados": FieldValue.arrayUnion(arregloInvitadosJson)});

  // guardo la referencia  en mi usuario
  FirebaseFirestore.instance
      .collection("beneficiarios")
      .doc(auth.currentUser.uid)
      .update({"reservaActiva": referenciaReserva}).then(
          (value) => debugPrint("$db_string reserva realizada"));
}

createBeneficiarioFromAuth(User user) async {
  return await FirebaseFirestore.instance
      .collection("beneficiarios")
      .doc(user.uid)
      .set({"email": user.email, "nombre": user.displayName}).then((value) {
    debugPrint(
        '$db_string se añadió un usuario firebase con el registro interno de Bienestar');
  }).catchError((e) {
    debugPrint("$db_string $e.toString()");
  });
}

beneExists(User user) async {
  DocumentSnapshot doc = await FirebaseFirestore.instance
      .collection("beneficiarios")
      .doc(user.uid)
      .get();

  doc.exists
      ? debugPrint("$db_string Usuario encontrado en tabla interna")
      : createBeneficiarioFromAuth(user);
}
