import 'package:bienestar/models/InterfazLista.dart';

import 'ControladoresBloqueHorario.dart';

// TODO: La disponibilidad por dia debe soportar un número arbitrario de bloques horarios.
/// Agrupa el inicio y el fin de un dia de [DisponibilidadHoraria].
class DisponibilidadPorDia implements InterfazLista {
  String _inicio;
  String _fin;

  String get inicio => _inicio;
  String get fin => _fin;

  DisponibilidadPorDia(this._inicio, this._fin);

  DisponibilidadPorDia.fromMap(Map<String, dynamic> map) {
    _inicio = map["inicio"];
    _fin = map["fin"];
  }
  DisponibilidadPorDia.fromControladores(ControladoresBloqueHorario rango) {
    _inicio = rango.inicio.text;
    _fin = rango.fin.text;
  }
  getTitle() {
    return "$inicio - $fin";
  }

  getSubtitle() {
    return "algo";
  }

  getTrailing() {
    return "algo";
  }

  String trailingString = "día: ";
  Map<String, dynamic> toJson() => {'inicio': inicio, 'fin': fin};
  String toString() {
    return "$inicio - $fin";
  }
}
