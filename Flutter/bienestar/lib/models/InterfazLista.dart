/// Interfaz utilizada para que una clase pueda ser
///representado en un [ListTile];
abstract class InterfazLista {
  String getTitle();
  String getSubtitle();
  String getTrailing();

  final String trailingString;

  InterfazLista(this.trailingString);
}
