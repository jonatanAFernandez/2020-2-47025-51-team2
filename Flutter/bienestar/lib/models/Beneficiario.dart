import 'package:bienestar/models/InterfazLista.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Beneficiario implements InterfazLista {
  String _nombre;
  String email;
  DocumentReference reference;
  get nombre => _nombre;

  Beneficiario.fromMap(Map<String, dynamic> map, {this.reference}) {
    _nombre = map["nombre"];
    email = map["email"];
  }

  Beneficiario.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), reference: snapshot.reference);

  Map<String, dynamic> toJson() => {"nombre": nombre, "email": email};

  String toString() {
    if (nombre != null) return nombre;
    if (nombre == null) return email;
  }

  @override
  String getSubtitle() {
    return email;
  }

  @override
  String getTitle() {
    return nombre;
  }

  @override
  String getTrailing() {
    return "Quitar esto";
  }

  @override
  String get trailingString => "Estado:";
}
