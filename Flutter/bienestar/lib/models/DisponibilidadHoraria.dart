import 'DisponibilidadPorDia.dart';

/// Almacena en un [Map] la [DisponibilidadPorDia] para cada día;
class DisponibilidadHoraria {
  Map<String, dynamic> _dias;
  Map<String, dynamic> get dias => _dias;

  DisponibilidadHoraria(
      DisponibilidadPorDia lunes, martes, miercoles, jueves, viernes, sabado) {
    _dias = {
      "lunes": lunes.toJson(),
      "martes": martes.toJson(),
      "miercoles": miercoles.toJson(),
      "jueves": jueves.toJson(),
      "viernes": viernes.toJson(),
      "sabado": sabado.toJson()
    };
  }

  DisponibilidadHoraria.fromMap(Map<String, dynamic> map) {
    _dias = map['dias'];
  }

  Map<String, dynamic> toJson() => {"dias": dias};
}
