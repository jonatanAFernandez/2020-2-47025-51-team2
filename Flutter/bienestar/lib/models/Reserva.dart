import 'package:cloud_firestore/cloud_firestore.dart';
import './Espacio.dart' show Espacio;
import '../models/InterfazLista.dart';
import '../models/Beneficiario.dart';

const db_string = "[Reserva class]";

class Reserva implements InterfazLista {
  Espacio _elementoReservado;
  String _beneficiario;
  List<Beneficiario> invitados;
  Espacio get elementoReservado => _elementoReservado;
  String get beneficiario => _beneficiario;
  String uid;
  DocumentReference reference;
  DateTime horaAgendada;

  String trailingString = "Invitados";

  setBeneficiario(String beneficiario) {
    _beneficiario = beneficiario;
  }

  setHoraAgendada(DateTime date) {
    horaAgendada = date;
  }

  String toString() {
    return "[Reserva de $elementoReservado a  $beneficiario]";
  }

  Map<String, dynamic> toJson() => {
        'elemento': elementoReservado.toJson(),
        'espacio_id': elementoReservado.reference.id,
        'usuario': beneficiario,
        'uid': uid,
        'horaAgendada': horaAgendada.toString(),
      };
  Reserva(this._elementoReservado, this._beneficiario);
  Reserva.conAgendamiento(this._elementoReservado, this._beneficiario, this.uid,
      this.horaAgendada) {
    invitados = <Beneficiario>[];
  }

  Reserva.fromMap(Map<String, dynamic> map, {this.reference}) {
    invitados = List<Beneficiario>();
    _elementoReservado = Espacio.fromMap(map['elemento']);
    _beneficiario = map['usuario'];
    horaAgendada = DateTime.parse(map["horaAgendada"]);

    if (map["invitados"] != null) {
      inicializarArregloInvitados(invitados, map['invitados']);
    }
  }

  Reserva.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), reference: snapshot.reference);

  getTitle() {
    String usuario = beneficiario;
    return usuario;
  }

  @override
  String getSubtitle() {
    String espacio = elementoReservado.nombre;
    return espacio;
  }

  @override
  String getTrailing() {
    String capacidad = "${elementoReservado.capacidadMaxima}";
    return capacidad;
  }
}

///Itera por cada elemento del arreglo de Firestore, inicializa un [Beneficiario]
///y lo añade a los invitados de la reserva
inicializarArregloInvitados(
    List<Beneficiario> arregloFinal, List arregloFirestore) {
  for (var i = 0; i < arregloFirestore.length; i++) {
    arregloFinal.add(Beneficiario.fromMap(arregloFirestore[i]));
    print(
        "$db_string se encontró un beneficirio ${arregloFirestore[i]["nombre"]} en reserva ");
  }
}
