class Subelemento {
  String _nombre;
  String _descripcion;
  bool _disponibilidad;

  String get nombre => _nombre;
  String get descripcion => _descripcion;
  bool get disponibilidad => _disponibilidad;

  set setNombre(String nombre) {
    _nombre = nombre;
  }

  set setDescripcion(String descripcion) {
    _descripcion = descripcion;
  }

  Subelemento(this._nombre, this._descripcion, this._disponibilidad);
  Map<String, dynamic> toJson() => {
        'nombre': nombre,
        'descripcion': descripcion,
        'disponibilidad': disponibilidad
      };
}
