import 'package:cloud_firestore/cloud_firestore.dart';
import 'DisponibilidadHoraria.dart';
import 'InterfazLista.dart';
import 'DisponibilidadPorDia.dart';
import 'Subelemento.dart';

class Espacio implements InterfazLista {
  String _nombre;
  String _descripcion;
  String _recomendaciones;
  int _capacidadMaxima;
  int _unidades;
  DisponibilidadHoraria _horario;
  List<Subelemento> subelementos;
  DocumentReference reference;
  String trailingString = "Capacidad";
  List<DisponibilidadPorDia> listaDisponibilidad;

  setNombre(String nombre) {
    _nombre = nombre;
  }

  setDescripcion(String descripcion) {
    _descripcion = descripcion;
  }

  setRecomendaciones(String recomendaciones) {
    _recomendaciones = recomendaciones;
  }

  setCapacidadMaxima(int capacidad) {
    _capacidadMaxima = capacidad;
  }

  setUnidades(int unidades) {
    _unidades = unidades;
  }

  setHorario(DisponibilidadHoraria horario) {
    _horario = horario;
  }

  List<DisponibilidadPorDia> darBloquesHorarios() {
    List<DisponibilidadPorDia> lista = [];

    // ignore: unused_local_variable
    DisponibilidadPorDia nuevo;
    lista.add(nuevo = DisponibilidadPorDia.fromMap(horario.dias["lunes"]));
    lista.add(nuevo = DisponibilidadPorDia.fromMap(horario.dias["martes"]));
    lista.add(nuevo = DisponibilidadPorDia.fromMap(horario.dias["miercoles"]));
    lista.add(nuevo = DisponibilidadPorDia.fromMap(horario.dias["jueves"]));
    lista.add(nuevo = DisponibilidadPorDia.fromMap(horario.dias["viernes"]));
    lista.add(nuevo = DisponibilidadPorDia.fromMap(horario.dias["sabado"]));
    return lista;
  }

// getters
  String get nombre => _nombre;

  String get descripcion => _descripcion;
  String get recomendaciones => _recomendaciones;
  int get capacidadMaxima => _capacidadMaxima;
  int get unidades => _unidades;
  DisponibilidadHoraria get horario => _horario;
  // List<Subelemento> get subelementos => _subelementos;

  // Para imprimir o pasar a Firestore
  Map<String, dynamic> toJson() => {
        'nombre': nombre,
        'descripcion': descripcion,
        'recomendaciones': recomendaciones,
        'capacidadMaxima': capacidadMaxima,
        'unidades': unidades,
        'horario': horario
            .toJson() //es necesario hacer el llamado al toJson(). De lo contrario el codec de Firestore no reconoce la referencia.
      };
  //Para instanciar un Espacio a partir de un Map, que retorna Firestore
  String toString() {
    return "espacio: $nombre";
  }

  Espacio.fromMap(Map<String, dynamic> map, {this.reference}) {
    _nombre = map['nombre'];
    _descripcion = map['descripcion'];
    _recomendaciones = map['recomendaciones'];
    _capacidadMaxima = map['capacidadMaxima'];
    _unidades = map['unidades'];
    _horario = DisponibilidadHoraria.fromMap(map[
        'horario']); // tiene que haber un constructor fromMap en disponibilidad Horaria
  }

  Espacio.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data(), reference: snapshot.reference);

  Espacio(this._nombre, this._descripcion, this._recomendaciones,
      this._capacidadMaxima, this._unidades, this._horario,
      {this.subelementos});

  //interfaz
  @override
  getTitle() {
    return nombre;
  }

  @override
  String getSubtitle() {
    return descripcion;
  }

  @override
  String getTrailing() {
    return "$capacidadMaxima";
  }
}
