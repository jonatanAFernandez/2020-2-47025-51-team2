import 'package:flutter/material.dart';

/// Agrupa dos [TextEditingController] para asociarlos a un [BloqueHorario].
/// Se usa en el [ModuloHorario] para construir el Widget,
class ControladoresBloqueHorario {
  TextEditingController inicio;
  TextEditingController fin;

  ControladoresBloqueHorario() {
    inicio = TextEditingController();
    fin = TextEditingController();
  }

  Map<String, dynamic> toJson() => {"inicio": inicio.text, "fin": fin.text};
}
