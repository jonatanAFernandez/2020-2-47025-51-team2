import 'package:flutter/material.dart';

class EspacioFormControllers {
  // Si los campos de Espacio cambian, se deben modificar aquí:

  TextEditingController nombre;
  TextEditingController descripcion;
  TextEditingController recomendaciones;
  TextEditingController capacidadMaxima;
  TextEditingController unidades;

  EspacioFormControllers() {
    nombre = TextEditingController();
    descripcion = TextEditingController();
    recomendaciones = TextEditingController();
    capacidadMaxima = TextEditingController();
    unidades = TextEditingController();
  }
}
