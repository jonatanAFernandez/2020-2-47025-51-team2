import 'package:flutter/material.dart';
import '../models/InterfazLista.dart';

/// Item de lista básico para utilizar dentro de un [ListView]. Requiere
/// que el elemento que desee mostrar implmente la [InterfazLista].
class ItemLista extends Container {
  final Widget roleOptions;
  final InterfazLista element;

  ItemLista(this.element, {this.roleOptions});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Column(children: [
        ListTile(
          title: Text(element.getTitle()),
          subtitle: Text(element.getSubtitle()),
          trailing:
              Text("${element.trailingString} : ${element.getTrailing()}"),
          // onTap: () => print(record),
        ),
        this.roleOptions != null
            ? roleOptions
            : Text('Role Options Placeholder')
      ]),
    );
  }
}
