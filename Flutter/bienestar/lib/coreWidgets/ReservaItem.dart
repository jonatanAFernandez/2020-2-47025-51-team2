import 'dart:ui';

import 'package:flutter/material.dart';
import '../models/Reserva.dart';
import 'package:intl/intl.dart';

class ReservaItem extends Container {
  final Widget roleOptions;
  final Reserva reserva;

  ReservaItem(this.reserva, {this.roleOptions});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Column(children: [
        ListTile(
            title: Text(reserva.getTitle()),
            subtitle: Text(reserva.getSubtitle())),
        ListTile(
          title: Text(
            "${DateFormat('MMM d h:mm a').format(reserva.horaAgendada)}",
            style: TextStyle(fontFamily: 'Lato', fontStyle: FontStyle.italic),
          ),
          trailing: Icon(Icons.date_range),
        ),
        this.roleOptions != null
            ? roleOptions
            : Text('Role Options Placeholder')
      ]),
    );
  }
}
