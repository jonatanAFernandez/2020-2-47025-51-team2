import 'package:flutter/material.dart';
import 'AristaBloqueHorario.dart';
import 'AristaBloqueHorarioEditable.dart';
import '../models/DisponibilidadHoraria.dart';

/// Un [BloqueHorario] editable. Retorna dos [AristaBloqueHorarioEditable].
// ignore: must_be_immutable
class BloqueHorarioEditable extends StatelessWidget {
  AristaBloqueHorario inicio;

  AristaBloqueHorario fin;
  String dia;

  BloqueHorarioEditable.fromDisponibilidad(
    DisponibilidadHoraria horario,
    this.dia,
    inicioController,
    finController,
    validator,
  ) {
    inicio = AristaBloqueHorario(
        dia, horario.dias[dia]['inicio'], inicioController, validator);
    fin = AristaBloqueHorario(
        dia, horario.dias[dia]['fin'], finController, validator);
  }

  build(BuildContext context) {
    return Column(
      children: [
        ListTile(title: Text(dia)),
        AristaBloqueHorarioEditable(
            inicio.arista, inicio.controller, inicio.validator),
        AristaBloqueHorarioEditable(fin.arista, fin.controller, fin.validator)
      ],
    );
  }
}
