import 'package:flutter/material.dart';

///Un [AristaBloqueHorario] que se construye a partir de una arista (inicio/fin)
///ya existente. Ideal para mostrar y permitir el cambio del valor exitente.
class AristaBloqueHorarioEditable extends StatelessWidget {
  final String horaExistente;

  final TextEditingController controller;
  final Function validator;

  AristaBloqueHorarioEditable(
      this.horaExistente, this.controller, this.validator);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        validator: validator,
        controller: controller,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: '$horaExistente',
          hintText: 'HH:MM',
        ));
  }
}
