import 'package:flutter/material.dart';

///Asocia un [TextFormField] básico a un controlador y una
///función de validación
class ControllerFormField extends StatelessWidget {
  final String campo;
  final TextEditingController controlador;
  final Function validator;

  ControllerFormField(this.campo, this.controlador, this.validator);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        controller: controlador,
        validator: validator,
        decoration: InputDecoration(
          labelText: campo,
          hintText: 'Ingresar ${campo.toLowerCase()}',
        ));
  }
}
