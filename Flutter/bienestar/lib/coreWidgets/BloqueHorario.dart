import 'package:flutter/material.dart';
import 'AristaBloqueHorario.dart';

///Agrupo dos [AristaBloqueHorario] para formar un [BloqueHorario].
// ignore: must_be_immutable
class BloqueHorario extends StatelessWidget {
  AristaBloqueHorario inicio;
  AristaBloqueHorario fin;
  String dia;

  BloqueHorario(this.dia, TextEditingController inicioController,
      TextEditingController finController, Function validator) {
    inicio = AristaBloqueHorario(dia, 'Inicio', inicioController, validator);

    fin = AristaBloqueHorario(dia, 'Fin', finController, validator);
  }

  build(BuildContext context) {
    return Column(
      children: [
        Padding(padding: EdgeInsets.all(15)),
        Container(
          child: Text(
            dia,
          ),
          alignment: Alignment.bottomLeft,
        ),
        inicio,
        fin
      ],
    );
  }
}
