import 'package:flutter/material.dart';

///Envoltura de un [TextFormField] que corresponde a una arista (inicio/fin)
///de un [BloqueHorario], para un día dado. El día es tan solo para mostrar
///en el label y el hint del [TextFormField]
class AristaBloqueHorario extends StatelessWidget {
  final String dia;
  final String arista;
  final TextEditingController controller;
  final Function validator;

  AristaBloqueHorario(this.dia, this.arista, this.controller, this.validator);
  //TODO: [Camilo] Implementar DateTimePicker en vez de TextFormField.
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        validator: validator,
        controller: controller,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: '$arista $dia',
          hintText: 'HH:MM',
        ));
  }
}
