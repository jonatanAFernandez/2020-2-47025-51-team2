import 'package:bienestar/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import './pages/consultar.espacios.dart';
import 'main.dart';
import './pages/reserva.activa.dart';

class BeneHome extends StatefulWidget {
// La clase Home tiene como funcón principal mostrar otros widgets en el navigationBar
// Por ello guarda una lista de widgets que desplegar

  BeneHome({Key key}) : super(key: key);

  @override
  _BeneHomeState createState() => _BeneHomeState();
}

class _BeneHomeState extends State<BeneHome> {
  var _selectedIndex = 0;
  static const db_string = "[BeneHome]";
  final _beneScaffoldKey = GlobalKey<ScaffoldState>();

  MaterialPageRoute login = MaterialPageRoute(
    builder: (context) => Scaffold(
      appBar: AppBar(title: Text("Iniciar Sesión")),
      body: LoginPage(),
      resizeToAvoidBottomPadding: false,
    ),
  );
//Lista de widgets que seleccionar desde el bottomNavigationBar
  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      // Container(
      //   alignment: Alignment.center,
      //   child: Text('TODO responsable: Felipe'),
      // ),
      ReservaActiva(),
      ConsultarEspacios(_beneScaffoldKey),
      Container(
        alignment: Alignment.center,
        child: Text('TODO responsable: no especificado'),
      ),
    ];

    // loadDummyToFirestore();
    var currentUser;
    if (auth.currentUser != null) {
      currentUser = auth.currentUser.email;
    } else {
      currentUser = "Usuario";
    }
    var logout = Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      alignment: Alignment.center,
      child: GestureDetector(
          onTap: () {
            FirebaseAuth.instance.signOut();
            Navigator.pushReplacement(context, login);
          },
          child: Text(
            "Sign out",
            style: TextStyle(decoration: TextDecoration.underline),
          )),
    );

    var usuario = Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        alignment: Alignment.center,
        child: Text(
          currentUser,
        ));

    return Scaffold(
      key: _beneScaffoldKey,
      appBar: AppBar(title: Text('Dashboard'), actions: [
        usuario,
        logout,
      ]),
      body: _widgetOptions[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.add), title: Text('Mi reserva')),
          BottomNavigationBarItem(
              icon: Icon(Icons.list), title: Text('Consultar')),
          BottomNavigationBarItem(
              icon: Icon(Icons.schedule), title: Text('Invitaciones')),
        ],
        currentIndex: _selectedIndex,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
            debugPrint("$db_string índice cambia a: $_selectedIndex");
          });
        },
      ),
    );
  }
}
