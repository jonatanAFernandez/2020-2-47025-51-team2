import 'package:bienestar/Modulos/firebaseOperations.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import './adminHome.dart';
import 'main.dart';

const dbg_string = "[LoginPage]";

/// Página con [TextFormField]s que se usan para correr el [FireBaseAuth]
/// al encontrar un usuario, reemplaza la vista actual con [AdminHome] o
/// [BeneficiarioHome]

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  var _formKey = GlobalKey<FormState>();

  TextEditingController email;
  TextEditingController password;

  String emailDado;

  LoginPageState() {
    email = TextEditingController();
    password = TextEditingController();
  }

  /// Retorna [AdminHome] o [BeneHome] dependiendo del email autenticado
  String navigationHandler(String userEmail) {
    var navRoute;
    userEmail == "admin@bienestar.com"
        ? navRoute = "/admin"
        : navRoute = "/bene";
    return navRoute;
  }

  @override
  Widget build(BuildContext context) {
    // Para guardar el con correcto del snackBar
    Function _authCorrecta(context) {
      return () {
        String navigationRoute = navigationHandler(auth.currentUser.email);

        debugPrint(
            "$dbg_string ruta de home para usuario ${auth.currentUser.email}: $navigationRoute");
        Navigator.pushReplacementNamed(context, navigationRoute);
      };
    }

    Function _snackbarNoUser(context) {
      return () {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text('El usuario no existe')));
      };
    }

    Function _snackbarWrongPsw(context) {
      return () {
        Scaffold.of(context).showSnackBar(
            SnackBar(content: Text('La contraseña es incorrecta')));
      };
    }

    var accionesAuth = [
      _authCorrecta(context),
      _snackbarNoUser(context),
      _snackbarWrongPsw(context),
    ];
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Form(
            key: _formKey,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                alignment: Alignment.bottomLeft,
                child: Text(
                  'Bienvenidos,',
                  style: TextStyle(
                      fontFamily: "Raleway",
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
              TextFormField(
                controller: email,
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Este campo no puede estar vacío';
                  } else {
                    var split = value.split('@');
                    if (split.length < 2) return 'No se encontró un dominio';
                  }

                  return null;
                },
                decoration: InputDecoration(labelText: 'Correo electrónico'),
                keyboardType: TextInputType.emailAddress,
              ),
              TextFormField(
                controller: password,
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Este campo no puede estar vacío';
                  }
                  return null;
                },
                decoration: InputDecoration(labelText: 'Contraseña'),
                keyboardType: TextInputType.visiblePassword,
                obscureText: true,
              ),
              Padding(padding: EdgeInsets.all(30)),
              RaisedButton(
                child: Text("Ingresar"),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    iniciarSesion(email.text, password.text, accionesAuth);
                  }
                },
              ),
              Padding(padding: EdgeInsets.all(15)),
              RaisedButton(
                child: Text(
                  "Google Sign-in",
                  style: TextStyle(fontFamily: "Lato"),
                ),
                onPressed: () {
                  signInWithGoogle(accionesAuth);
                },
              ),
            ])));
  }
}

/// Lógica principal de la autenticación de Firebase, las acciones a tomar
/// se pueden pasar en el arreglo [closures]
iniciarSesion(String email, String psw, var closures) async {
  debugPrint("$dbg_string se invocó iniciar sesión. Esperando respuesta");
  try {
    // ignore: unused_local_variable
    UserCredential userCredential = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: psw);
    beneExists(auth.currentUser);
    closures[0]();
  } on FirebaseAuthException catch (e) {
    if (e.code == 'user-not-found') {
      print('$dbg_string No user found for that email.');

      closures[1]();
    } else if (e.code == 'wrong-password') {
      print('$dbg_string Wrong password provided for that user.');
      closures[2]();
    }
  }
}

crearNuevaCuenta(FirebaseAuth instance, String email, String psw) async {
  try {
    await instance.createUserWithEmailAndPassword(email: email, password: psw);
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      print('The password provided is too weak.');
    } else if (e.code == 'email-already-in-use') {
      print('The account already exists for that email.');
    }
  } catch (e) {
    print(e);
  }
}

signInWithGoogle(var closures) async {
// Trigger the authentication flow
  final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

  // Obtain the auth details from the request
  final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

  // Create a new credential
  final GoogleAuthCredential credential = GoogleAuthProvider.credential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );

  // Once signed in, return the UserCredential
  await FirebaseAuth.instance.signInWithCredential(credential);
  beneExists(auth.currentUser);
  closures[0]();
}
