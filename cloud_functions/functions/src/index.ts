import * as functions from 'firebase-functions';


const admin = require('firebase-admin');
admin.initializeApp()
const db = admin.firestore();
const nodemailer = require('nodemailer');

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info("Hello logs!", {structuredData: true});
  response.send("Esta es tu primera cloud function!");
  console.log("Esta es tu primera cloud function!");
});




export const reservaOnDeleteTrigger = functions.firestore
  .document('reservas/{docId}')
  .onDelete((snap, context) =>{
      // guardo id del beneficiario
      const reserva = snap.data();
      console.log("el uid para esta reserva es:" + reserva["uid"]);
      db.collection('beneficiarios').doc(reserva["uid"]).update(
         {"reservaActiva": false}
).catch( (e: any)=>{
    console.log('bloque del catch');
console.log(e);
}).then(

   (value: any)=>{
       console.log(value);
   } 
)

return;

      });


      export const espacioOnDeleteTrigger = functions.firestore
  .document('espacios/{docId}')
  .onDelete((snap, context) =>{
      // guardo id del espacio
      const espacio = snap.id;
      console.log("el uid para esta espacios es:" + espacio);
      
      db.collection('reservas').where("espacio_id", "==", espacio).get()
      .then(function(querySnapshot: any) {
          querySnapshot.forEach(function(reserva: any) {
              console.log("se encontró que la reserva " +reserva.id + " estaba asociada al espacio "+ espacio );
              console.log("Empezando el proceso de eliminación")
              // Borro la reserva asociado a ese especio
              db.collection("reservas").doc(reserva.id).delete().then((value: any) => console.log("lo que pasa cuando se borra la reserva" + reserva.id+ "es :"+ value))
              .catch((error: Error) => console.log("hubo un problema eliminando la reserva " + reserva.id));
          });
      })
      .catch(function(error: Error) {
          console.log("Hubo un problema ejecutando la sentencia where(\"espacio_id\", \"==\", espacio): " + error);
      });
  
  ;
  

      });

// Esta parte es experimental 
// =========================

// al parecer esta parte configura el ambiente en la nube 
const gmailEmail = "jahumaf@gmail.com"; // no entiendo qué email y psw se están asignando aquí
const gmailPassword = "utxylvlewontecpd";

// las variables de entorno del proyecto de firebase 
// se usan para configurar el mailTransport de nodemailer
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});
const APP_NAME = 'Bienestar K';

export const sendWelcomeEmail = functions.auth.user().onCreate((user) => {
 
      const email = user.email; // The email of the user.
      const displayName = user.displayName; // The display name of the user.
 
      const mailOptions = {
        from: `${APP_NAME} <noreply@firebase.com>`,
        to: email,
        subject:  `Bienvenido(a) a ${APP_NAME}!`,
        text: `Hola ${displayName || ''}! Bienveinido a ${APP_NAME}. I hope you will enjoy our service.`,
      };
     
    
      mailTransport.sendMail(mailOptions);
      console.log('se envió mensaje a:', email);
      return null;

    });

    export const notificarInvitacionPorCorreo = functions.firestore.document('reservas/{docId}').onUpdate((snap, context) => {
      // 1. tomo los emails del documento en reservas/
      const emails = snap.after.data()["invitados"];
      console.log("El snap.data() es: " + JSON.stringify(snap.after.data()));
      console.log(emails);
      let mailToString ="";

      // 2. formo cadena de correos separados con comas hasta el penultimo en la lista
      for (var i = 0; i < emails.length - 1; i++) {
        mailToString = mailToString + emails[i]["email"] +",";
      }
     
      // 3. añado el último en la lista sin ninguna coma
      mailToString = mailToString + emails[emails.length-1]["email"];

      // 4. escribo el correo
      
      // let textoDelCorreo = `Fuiste a ${snap.after.data()["elemento"]["nombre"]} en ${snap.after.data()["horaAgendada"]} por ${snap.after.data()["usuario"]}`;
      let textoDelCorreo = `Tienes el gran honor de haber sido invitado a una reunión en Bienestar. Revisa el dashboard de tu app Bienestar K`;

      const mailOptions = {
        from: `${APP_NAME} <noreply@firebase.com>`,
        to: mailToString,
        subject:  `Estimados ` + mailToString,
        text: textoDelCorreo,
      };

     // 5. pruebo las variables
      console.log("texto del correo: " + textoDelCorreo);
      console.log("recipientes del correo: " + mailToString);

      mailTransport.sendMail(mailOptions)

      console.log("corrió nodemailer");
      return null;

    });

