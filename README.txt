Proyecto Bienestar Universitario
================================

Fundación Univeristaria Konrad Lorenz
-------------------------------------

Construcción de Aplicaciones Multiplataforma 31628 - Ingeniería de Software 1 47025
-----------------------------------------------------------------------------------

Aplicación móvil que permite a los estudiantes, funcionarios y docentes
de la FUKL apartar espacios comunes y elementos del Bienestar
Univerisatorio desde su dispositivos.

### Integrantes

-   Jonatan Ahumada Fernández
-   Geoffrey Soto Soto
-   Cristian Martínez
-   Juan Felipe Betancourt


tes de llegar al semáforo cruzar a mano izquierda 
- 

Reglas de calidad
-----------------

-   *No* hacer push al master
-   Entregar, eso si sus cambios partiendo sobre la versión del master
    al inicio del Sprint
-   Si empezó a trabajar antes de que la versión master estuviera
    consolidada, usted debe hacer primero el 'pull' del master, fusionar
    correctamente lo que hubiere lugar y luego empujar a la rama que le
    corresponda. Nunca olivde: *No* hacer push al master
-   Enviaré por correo cuál es la versión que se considera estable al
    inicio de cada sprint
-   No sea un mediocre y entienda la herramienta git antes de preguntar
    o hacer algo estúpido: https://git-scm.com/book/es/v2
-   Guardar clases en archivos discretos. Esto ayuda a evitar conflictos
-   Se deben primero revisar las clases ya hechas antes de escribir
    nuevas.
-   Widgets van en '/coreWidgets'
-   clases solas van en '/models'
-   los modulos son agrupaciones de widgets y clases reutilizables
-   No utilizar código que no entiendan. No queremos inyectar widgets
    innecesarias al árbol, ni utilizar propiedades cuyos efectos
    desconozcamos. Esto es fuente de potenciales errores.
-   *No* decir que algo 'está listo' sin haberlo probado antes.
-   *respetar* la signatura de la clase o función que se les asignó
-   Se debe documentar cada clase o función que se haga para que su
    compañero lo pueda entender. Se usarán los dart doc comments
    (https://dart.dev/guides/language/effective-dart/documentation)
-   si necesitan cargar datos de prueba. Descomenten la funcion
    `loadDummyData` en el main. Lógicamente configúrenla en
    `lib/dummy.data.dart`
-   eliminar las ramas en el remoto una vez cumplan su función y se
    hayan fusionado al master. Eso hace más facil leer la historia del
    proyecto.

Q & A
-----

-   Es necesario/aconsejable enlazar (issue link) unas historias con
    otras en el tablero de Jira cuando las relaciones son claras? Por
    ejemplo, si una historia se desglosa en otras 5, estas deben tener
    el 'Split from'?

R. Sí. Eso nos ahorró trabajo más tarde

-   ¿Es apropiado construir los servicios bajo el supuesto de que
    implican una petición http a una url que retorna datos en formato
    json?

R. No propiamente. Utilizamos Firestore que utiliza como interfaz un Map(String, dynamic)
-----------------------------------------------------------------------------------------

Rutas
-----

    root
        admin
             crear
             editar
             reservas



        beneficiario
              reserva
              invitaciones
